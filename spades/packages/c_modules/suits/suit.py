#!/bin/env python3

#This was originally part of the cards.py, but has been moved to test path additions.

#Find suit type

def suit_type(card):
	if(card == 1):
		return "Heats"
	elif(card == 2):
		return "Diamonds"
	elif(card == 3):
		return "Clubs"
	elif(card == 4):
		return "Spades"
