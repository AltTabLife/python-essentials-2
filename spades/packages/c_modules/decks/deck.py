#!/bin/env python3

#Custom module for creating decks and drawing cards



#Counters
decks_generated = 0


#Deck Generation
def gen_deck():
	global decks_generated
	decks_generated +=1
	
	s = []

	for i in range(1, 5):
		for x in range(1,14):
			s.append((i, x))
	return s



